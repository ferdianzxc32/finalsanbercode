import 'package:flutter/material.dart';

class DrawerScreen extends StatefulWidget {
  const DrawerScreen({Key? key}) : super(key: key);

  @override
  State<DrawerScreen> createState() => _DrawerScreenState();
}

class _DrawerScreenState extends State<DrawerScreen> {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: [
          const UserAccountsDrawerHeader(
            accountName: Text("Muhammad Ferdian Pradana "),
            currentAccountPicture: CircleAvatar(
              backgroundImage: AssetImage("assets/images/ferdian.jpg"),
            ),
            accountEmail: Text("ferdianzxc32@gmail.com"),
          ),
          DrawerListTile(
            iconData: Icons.newspaper,
            title: "Berita HOT",
            onTilePressed: () {},
          ),
          DrawerListTile(
            iconData: Icons.newspaper,
            title: "Berita Luar Negeri",
            onTilePressed: () {},
          ),
          DrawerListTile(
            iconData: Icons.newspaper,
            title: "Berita Hari Ini",
            onTilePressed: () {},
          ),
          DrawerListTile(
            iconData: Icons.newspaper,
            title: "Berita Boxing",
            onTilePressed: () {},
          ),
          DrawerListTile(
            iconData: Icons.newspaper,
            title: "Berita Teknologi",
            onTilePressed: () {},
          ),
          DrawerListTile(
            iconData: Icons.settings,
            title: "Settings",
            onTilePressed: () {
            },
          ),
        ],
      ),
      backgroundColor: Color.fromARGB(255, 130, 143, 180),
    );
  }
}

class DrawerListTile extends StatelessWidget {
  final IconData iconData;
  final String? title;
  final VoidCallback? onTilePressed;

  DrawerListTile({
    Key? key,
    required this.iconData,
    this.onTilePressed,
    this.title,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      onTap: onTilePressed,
      dense: true,
      leading: Icon(iconData),
      title: Text(
        title!,
        style: TextStyle(fontSize: 16),
      ),
    );
  }
}
