import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';

class AboutMe extends StatefulWidget {
  const AboutMe({Key? key}) : super(key: key);

  @override
  State<AboutMe> createState() => _AboutMeState();
}

class _AboutMeState extends State<AboutMe> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromARGB(255, 173, 151, 51),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children:[
            SizedBox(
              height: 60,
            ),
            Image.asset(
                "assets/images/ferdian.jpg",
                height: 150,
                ),
            SizedBox(
              height: 10,
            ),
            Text(
              "Muhammad Ferdian Pradana",
              style: TextStyle(
                color: Color.fromARGB(255, 0, 0, 0),
                fontSize: 30,
                fontWeight: FontWeight.w700,
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Container(
              color: Colors.black,
              height: 10,
            ),
            Container(
              height: 20,
            ),
            Text(
              "Nama: Muhammad Ferdian Pradana"
              "          "
              "Asal: Kalimantan Timur"
              "                                 "
              "Email ferdianzxc32@gmail.com",
              style: TextStyle(
                color:const Color.fromARGB(255, 0, 0, 0),
                fontSize: 20,
                fontWeight: FontWeight.w700,
              ),
            ),
          ],
        ),
      ),
    );
    
  }
}